use std::f64::consts::PI;

fn main() {
    // print!("\x1B[2J");
    let a = PI / 4.0;
    let b = PI / 3.0;
    let c = PI / 6.0;

    let cube_width = 10;
    let width = 160;
    let height = 44;
    let mut z_buffer = [160 * 44];
    let mut buffer = [160 * 44];
    let background_unicode = ' ';
    //gonnna have to check out how rust unicode works, as ASCII is old

    let vec = vec![0u8; 10];

    let x = calculate_x(1.0, 2.0, 3.0, a, b, c);
    let y = calculate_y(1.0, 2.0, 3.0, a, b, c);
    let z = calculate_z(1.0, 2.0, 3.0, a, b, c);

    println!("x = {}", x);
    println!("y = {}", y);
    println!("z = {}", z);
}

fn calculate_x(i: f64, j: f64, k: f64, a: f64, b: f64, c: f64) -> f64 {
    return (
        j * (a.sin() * b.sin() * c.cos()) -
        k * (a.cos() * b.sin() * c.sin()) +
        j * (a.cos() * c.sin()) +
        k * (a.sin() * c.sin()) +
        i * (b.cos() * c.cos())
    );
}

fn calculate_y(i: f64, j: f64, k: f64, a: f64, b: f64, c: f64) -> f64 {
    return (
        j * (a.cos() * c.cos()) +
        k * (a.sin() * c.cos()) -
        j * (a.sin() * b.sin() * c.sin()) +
        k * (a.cos() * b.sin() * c.sin()) -
        i * (b.cos() * c.sin())
    );
}

fn calculate_z(i: f64, j: f64, k: f64, a: f64, b: f64, c: f64) -> f64 {
    return k * (a.cos() * b.cos()) - j * (a.sin() * b.sin()) + i * b.sin();
}

fn execution() {
    while true {
        vec![buffer, background_unicode, width * height];
        vec![z_buffer, 0, width * height * 4];
    }
}
